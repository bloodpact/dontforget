import buyList from "@/store/modules/buy-list";
import recipes from "@/store/modules/recipes";

export const modules = {
    buyList,
    recipes,
};
