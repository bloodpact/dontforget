const recipes = {
    namespaced: true,
    state: () => ({
        recipes: [{ id: 1, test: "test" }],
    }),
    mutations: {},
    actions: {},
    getters: {
        recipes: (s) => s.recipes,
    },
};
export default recipes;
